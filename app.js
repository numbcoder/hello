var envConfig = require('./app/config/env.json');
var config = require('./app/config/'+envConfig.env+'/config');
var Robot = require('./lib/robot').Robot;
var fs = require('fs');
var http = require('http');

var httpGlobalAgent = http.globalAgent;
httpGlobalAgent.maxSockets = 1500;

//
// node main dev master run service
//
var robot = new Robot(config);


var mode = 'master';

if (process.argv.length > 2){
    mode = process.argv[2];
}
 
if (mode !== 'master' && mode !== 'client') {
	throw new Error(' mode must be master or client');
}

if (mode==='master') {
    robot.runMaster(__filename);
} else {
    var script = (process.cwd() + envConfig.script);
    robot.runAgent(script);
}

process.on('uncaughtException', function(err) {
	console.error(' Caught exception: ' + err.stack);
	if (!!robot && !!robot.agent){
		robot.agent.socket.emit('crash',err.stack);
	}
	fs.appendFile('.log', err.stack, function (err) {});
});
