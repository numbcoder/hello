var EventEmitter = require('events').EventEmitter;
var io = require('socket.io-client');
var http = require('http');

if (typeof Object.create !== 'function') {
  Object.create = function(o) {
    function F() {}
    F.prototype = o;
    return new F();
  };
}

var pomelo = Object.create(EventEmitter.prototype); // object extend from object
var socket = null;
var id = 1;
var callbacks = {};
var route = "web-connector.messageHandler.";
var isRegister = false;
var success = 200;
var sdk_version = '0.1.0';
var register_ack = 'register';
var message_store = {};

/**
 * Initialize connection to server side
 *
 * @param {String} host server ip address
 * @param {Number} port server port
 * @param {Function} cb callback function
 * @memberOf pomelo
 */
pomelo.init = function(host, port, cb) {
  var url = 'http://' + host;
  if (port) {
    url += ':' + port;
  }
  socket = io.connect(url,{'force new connection': true, reconnect: true,
    'try multiple transports': false, 'max reconnection attemps': 50});
  // connect on server side successfully
  socket.on('connect', function() {
    console.log('[pomeloclient.init] socket.io connected!');
    cb();
  });

  socket.on('reconnect', function() {
    console.log('reconnect');
  });

  // receive socket message
  socket.on('message', function(data) {
    console.log('receive', data);
    var msg = null;
    if (typeof data === 'string') {
      msg = JSON.parse(data);
    }
    if (msg instanceof Array) {
      processMessageBatch(msg);
    } else {
      processMessage(msg);
    }  
  });

  // encounter connection error
  socket.on('error', function(err) {
    monitor('incr', 'connecterror');
    cb(err);
  });

  socket.on('disconnect', function(reason) {
    monitor('incr', 'disconnect');
    pomelo.emit('disconnect', reason);
  });
};

/**
 * Send request to server side
 *
 * @param {String} method request type include: bind & cancelBind & getOnlineUser
 * @param {Object} opts request parameters
 * @param {Function} cb callback function
 * @private
 */
var request = function(method, opts, cb) {
  // if method is not exist
  if (!method) {
    console.error("request message error with no method.");
    return;
  }
  id++;
  callbacks[id] = cb;
  sendMsg(method, opts);
};

/**
 * Encode message and send by socket
 *
 * @param {String} method request type include: bind & cancelBind & getOnlineUser
 * @param {Object} msg message need to send
 * @private
 */
var sendMsg = function(method, msg) {
  // assembly request route
  var path = route + method;
  var rs = {
    id: id,
    route: path,
    msg: msg
  };
  var sg = JSON.stringify(rs);
  try {
    socket.send(sg); 
  } catch(ex) {
    console.log('send error');
  }
};

/**
 * Process message in batch
 *
 * @param {Array} msgs message array
 * @private
 */
var processMessageBatch = function(msgs) {
  for (var i = 0, l = msgs.length; i < l; i++) {
    processMessage(msgs[i]);
  }
  for (var key in message_store) {
    pomelo.emit(key, message_store[key]);
  }
  message_store = {};
};

/**
 * Process message
 *
 * @param {Object} msg message need to process
 * @private
 */
var processMessage = function(data) {
  var msg = data;
  if (typeof data === 'string') {
      msg = JSON.parse(data);
  }
  if (msg.id) {
    //if have a id then find the callback function with the request
    var cb = callbacks[msg.id];
    delete callbacks[msg.id];

    if (typeof cb !== 'function') {
      console.log('[pomeloclient.processMessage] cb is not a function for request ' + msg.id);
      return;
    }

    if (!msg.body) {
      msg.body = msg;
    }

    cb(msg.body);
    return;
  }
  //if no id then it should be a server push message
  else {
    console.error('push message', msg);
    monitor('incr', msg.route);
    message_store[msg.route] = message_store[msg.route] || [];
    message_store[msg.route].push(msg.body);
  }
};


/**
 * Send domain information on server side
 *
 * @param {Object} opts include domain: product domain & productKey: product key
 * @param  {Function} cb callback function return data include code: response code
 * @memberOf pomelo
 */
pomelo.register = function(opts, cb) {
  monitor('incr', 'register');
  opts.sdk_version = sdk_version;
  request('register', opts, cb);
};


/**
 * Send user information on server side
 *
 * @param {Object} opts include domain: product domain & user: user id & signature: user signature & expire_time: signature expire time & nonce: random string & productKey: product key
 * @param  {Function} cb callback function return data include code: response code & user: user id
 * @memberOf pomelo
 */
pomelo.bind = function(opts, cb) {
  monitor('incr', 'bind');
  if (isRegister) {
    request('bind', opts, cb);
  } else {
    console.log('cannot bind without registration.');
  }
};

/**
 * Delete user information on server side
 *
 * @param {Object} opts include domain: product domain & user: user id
 * @param  {Function} cb callback function return data include code: response code & user: user id
 * @memberOf pomelo
 */
pomelo.cancelBind = function(opts, cb) {
  request('cancelBind', opts, cb);
};

/**
 * Query users status
 *
 * @param {Object} opts include domain: product domain & ids: user id array
 * @param  {Function} cb callback function return data like: {test1: 0, test2: 1}
 * @memberOf pomelo
 */
pomelo.getOnlineUser = function(opts, cb) {
  request('getOnlineUser', opts, cb);
};


pomelo.getOfflineMessage = function(opts, cb) {
  request('getOfflineMessage', opts, cb);
};

/**
 * Disconnect from server side
 *
 * @memberOf pomelo
 */
pomelo.disconnect = function() {
  if (socket) {
    socket.disconnect();
    socket = null;
  }
};

var monitor = function(type,name,reqId){
  if (typeof actor!='undefined') {
    actor.emit(type, name, reqId);
  } else {
    console.error(Array.prototype.slice.call(arguments,0));
  }
}

var host = '123.58.180.26';
// host = 'pomelo5.server.163.org';
// host = 'fkmm8.photo.163.org';
// host = "192.168.144.199";
// host = '127.0.0.1';
var port = 6003;
//port = 3031;
var uid = typeof actor!='undefined'?actor.id:-33;
// var user = 'testvvv' + uid;
var user = 'abc';
var randomV = Math.floor((Math.random() * 2) + 1);
if (randomV === 1) {
  user = 'py';
}

var username = user;

var domain = "test1.163.com";
var productKey = "4acf85f046e8439d8e2aadfc6bfe08e6";

var login_url = "http://123.58.180.180:8080/drill/login/index?account="+ user 
  + '&minutes=43200&productDomain=' + domain;
var success = 200;

var pomelo_client = pomelo;
var friendsId = ['xy','abc','py','testvvv99','testvvv98','testvvv94'];

var register = function() {
  monitor('start','register',2);
  console.time('register');
  var args = {
    domain: domain,
    productKey: productKey
  };
  pomelo.register(args, function(data) {
    console.timeEnd('register');
    console.log('data.code = ', data.code);
    if (data.code === success) {
      monitor('end','register',2);
      monitor('incr', 'registerack');
      isRegister = true;
      http.get(login_url, function(res) {
        res.setEncoding('utf-8');
        var buffer = '';
        res.on('data', function(chunk) {
          var nonce = chunk.split("&")[0].split("=")[1];
          var expire_time = chunk.split("&")[1].split("=")[1];
          var signature = chunk.split("&")[2].substring(10);
          bind2(nonce, expire_time, signature);
        });
      }).on('error', function(e) {
        console.log('get request error', e, e.stack);
      });
    } else {
      console.log('error', data);
      monitor('end', 'register', 2);
      monitor('incr', 'registerackerror');
    }
  });
};

var bind2 = function(nonce,expire_time,signature){
  console.log(nonce + ' ' + expire_time+ ' ' + signature);
  var msg = {
    user: user,
    nonce: nonce,
    expire_time: expire_time, 
    signature: signature,
    domain: domain,
    productKey: productKey
  };
  monitor('start', 'bind', 1);
  pomelo.bind(msg, function(data) {
    if (data.code === success) {
      monitor('end', 'bind', 1);
      monitor('incr', 'bindack');
      isLogined = true;
      console.log('bind ok');
      return;
    } else {
      monitor('end', 'bind', 1);
      monitor('incr', 'binderror');
      return;
    }
  });
};

setTimeout(function() {
  pomelo.init(host, port, register.bind(null));
}, Math.random() * 3000);

pomelo_client.on('specify', function(data) {
  console.log('specify', data);
});

pomelo_client.on('broadcast', function(data) {
  if (isLogined) {
    monitor('incr', 'broadcast');
    for (var i = 0; i < data.length; i++) {
      var data = JSON.parse(data[i].content);
      console.log(data);
    }
  }
});

process.on('uncaughtException', function(err) {
  console.error(' Caught exception: ' + err.stack);
});

